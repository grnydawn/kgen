\section{KGEN: Fortran Kernel Generator} \label{sec:implementation}

We next describe the tasks that KGEN perform as well as its user interface.  We begin by describing the manual extract of a computational kernel from a simple example.  This description sets the stage for a more formal description of the necessary analysis tasks and the respective components of KGEN that perform these tasks.    

\begin{figure}[h]
\centering
\includegraphics[width=3.in]{ORG_PROGRAM.png}
\caption{A sample program and corresponding syntax trees. Lines 40 to 47 is the kernel to be extracted while line 26 is the call-site.}
\label{fig:org-program}
\end{figure}

Consider the trivial program illustrated in Figure \ref{fig:org-program}.  This trivial example initializes Message Passing Interface (MPI) \cite{mpi}, calls a subroutine {\it update} that subsequently calls the subroutine {\it calc}.  For the purposes of discussion, we are interested in creating a computational kernel which represents the calculations perform in {\it calc}.  The first step in creating a {\it calc} kernel consists of capturing the input and output state of the {\it calc} subroutine.  This could be achieved by the addition of write statements both before and after the call to {\it calc} to write the state of the subroutine to a file.  Because this is an MPI application and the fact that {\it calc} is called multiple times from {\it update}, additional if-tests could be added to limit that amount of output state that is generated.  The modified code, with the addition of the write statements would be executed and the output state files would be saved.  

The next step would be the creation of driver code.  The driver code would  call the subroutine {\it calc} using the state data from the output state files.  Additional diagnostic code could be added to the driver code to provide timing and verification of the results. For this example program, the creation of a {\it calc} kernel would require a modest programmer effort.  However, the trivial example in Figure \ref{fig:org-program} bears little resemblance to the complexity that is typically found in large scientific applications.  For example, in CESM, a computational kernel may range in size from several hundred of lines to 10,000 lines and include a multi-level call tree.  The subroutine may contain 200+ arguments, and make extensive use of variables used in modules.  The manual extraction method described previously is simply not feasible for the creation of such a kernel.
% much less the creation of tens or hundreds of such kernels.
It is the motivation that inspired the creation of KGEN to automate the kernel creation process.  Figure \ref{fig:kgen-flow}  illustrates this process.  Using the original application source code as a starting point, KGEN generates both a new set of source code for the kernel files and the modified state files. The modified state files contain code to capture the input and output state. The original application is recompiled and rerun to capture state data that is then used by the new standalone kernel.   

\begin{figure}
\centering
\includegraphics[width=3.in]{KGEN_FLOW.png}
\caption{The KGEN workflow involves: the generation of instrumented and kernel source code from the original production application, execution of the instrumented application to generate state data, and the execution of the kernel.}
\label{fig:kgen-flow}
\end{figure}


% THIS description is really not Manual Kernel Extraction but rather the complexities of the who process.  I think this belongs elsewhere.  

%\subsubsection{Manual Kernel Extraction}


%This section explains manual process of kernel creation from an existing application. Major tasks in this manual process are the targets of automation in KGEN.

%Manual kernel extraction usually starts from selecting a region of code to be extracted. The region could be as simple as an inner-most Fortran Do loop, or as complex as a Fortran Function that contains large number of  calls to another Fortran Subprograms. To make a kernel as a standalone executable, copying a region of kernel codes alone is generally not enough. It is more common that the kernel uses information defined elsewhere such as external Fortran Module. Therefore, all the external information should be copied together with a code region of the kernel. Furthermore,  the external information may use another external information. Therefore this information collection task generally requires multiple-level searching. The most common task of the searching is to find Fortran type declaration of a variable. For example, if a Fortran Integer variable is used in a kernel but not declared locally, for instance, in Fortran Function, the information should be searched through namespace hierarchy or external modules. More complex case is when an external variable is Fortran derived type. As a derived type variable contains components of another type, it could be multiple-level searches to get all information to use the original external variable.
 
%Derived type is particularly important when it comes to save input/output data of the kernel. To drive kernel execution, input data to the kernel should be provided. When an input data is derived type, so-called "deep copy" of the data is required, where the content of components is copied up to the point that a component is not a derived type. Additional complexity could be added if the variable has pointer attribute or is a deferred array type. For example, if a variable is a pointer type, the variable may targets to another variable that is used in the extracted kernel and so-called "aliasing issue" may occur. One subtle but important task for saving input/output data for kernel is to resolve side-effects. Side-effect happens when some variables are used in the kernel but are not provided through Function or subroutine arguments. Those variables should be saved as both of input and output data together in addition to the arguments. Output data is also saved to be used for kernel verification. The extracted kernel compares these output data with output from the kernel execution for verification. Macros are commonly used in a large application. As saved data are generated based on a specific set of macro values, kernel should be extracted from preprocessed source files.

\subsection{Manual source analysis}

   We next provide a more systematic description of the type of syntax tree calculations in order to extract a kernel.  We utilize the essential principles of static backward program slicing to build KGEN. 
To motivate our discussion, we will use the same simple example in Figure \ref{fig:org-program}.
We want to extract
subroutine {\it calc} located between line no. 40 and 47 of "calc\_mod.F90". 
Syntax tree representation of each source files are shown to the right side
of the source code. Because a subroutine can be called from multiple locations in the program, a particular call-site must be specified, in this case, line 26 in the file ``update\_mode.F90."  
With the specification of the call-site, the correct values of each argument to {\it calc} can be captured.  

%call-sites, we must specify the particulaWhile we want to extract subroutine {\it calc}, a call-site to the {\it calc}
%should be know to an automatic kernel generator beforehand. This is because the subroutine
%can be called from multiple call-sites and each call-site might
%have different values for each arguments when {\it calc} is called. With a specific
%call-site given, the generator can collect correct value of argument.
%The {\it calc} subroutine will be eventually found by the generator through searching from the call-site.

The KGEN search algorithm or generator starts from a call-site statement. In this example, the location of
a call-site on line 26 of "update\_mod.F90" is indicated by the KGEN directive  on line 25.
The call-site is represented by a circle label with the line number 26 in the syntax tree in Figure~\ref{fig:org-program} .  
The generator then checks if any parent node of the tree can resolve the
subroutine name {\it calc.} Through the searching, the generator determines 
that a Fortran use statement of line 14 provides indirect
name resolution. As the name is defined in a module of a different source file,
the generator will read "calc\_mod.F90." The path to "calc\_mod.F90'' should
be provided by user in order for generator to properly locate the source file. Once
 kernel is found, the generator reads each statement in the kernel
 to determine if additional name resolution is required. If required,
 the generator resumes name resolution until all necessary names are discovered. 
 
Necessary input and output data for the kernel can be statically
analyzed by collecting information from both of call-site and
intent attributes of arguments in the kernel. Intent attributes of "IN" or "INOUT" require the collection of input variables.  
In Figure~\ref{fig:org-program}, ''i'' and ''j'' in calc
subroutine of calc\_mod.F90 are input variables.  
In similar way, "OUT" or "INOUT" variable in update subroutine
of update\_mod.F90 is marked as an output variable. If there is not intent attribute specified, 
the variable is considered as having INOUT attribute. Input data are
saved before calling a kernel and output data are saved after the subroutine call. 
If arguments are derived types, each component of derived types are save recursively. 
Based on the variables intent, the generator will add binary write calls to the original source code to enable state data collection.  
Once the instrumented source code is generated, the full application is recompiled with the new source code and rerun to collect input/output state data.  

The generated kernel source code consists of a driving routine which calls the kernel subroutine {\it calc}.  An additional set of calls are added to read in the 
state data, to measure timing of the kernel execution and to verify that the output calculated by the kernel matches the expected output generated by the full application.  


%To drive a kernel execution, additional codes to call the call-site is
%required. The main function of the driving code is to read input
%data and populate them into argument variables and to verify output
%from the kernel execution by comparing them with reference output
%data read from file system.


\subsection{Automated source analysis}

\begin{figure}
\centering
\includegraphics[width=3.in]{KGEN_OP.png}
\caption{KGEN marks all Fortran statements across multiple source files and extracts them as a stand-alone executable.
The extraction involves name resolving, instrumenting for data generation and verification.}
\label{fig:kgen-op}
\end{figure}

 
%KGEN operation is shown in Figure~\ref{fig:kgen-op}. KGEN operation starts when it receives call-site information through command-line or directive interface. Call-site contains information of the location of a Fortran statement in a Fortran source file that it should be analysed first.

The starting point for KGEN analysis shown in Figure~\ref{fig:kgen-op} is the identification of the call-site.
KGEN then pre-process the given source file using external the Intel Fortran pre-processor, fpp~\cite{intelfpp},  or the GNU C pre-processor, cpp~\cite{gnucpp}.
To properly pre-process, KGEN should be given a complete
set of macros used in the source file. Without correct macro definitions,
KGEN can not decide which parts of codes should be used for kernel extraction.
In addition, if the source file needs to import external files, inclusion paths also
should be provided.
The preprocessed source file is handed to F2PY parser and, in turn,
KGEN Name Resolver starts to resolve names used in kernel based on parsed results
from F2PY parser. If a name should be resolved
by an external module that is not parsed yet, KGEN tries to locate
and parse it based on paths provided by user. Name resolution is
bottom-up search of a syntax tree from the statement node having unresolved name to
a statement that resolves the name. And the process
repeats until there is no unresolved name. Arguments to the kernel
function or subroutine are special names in that the content of
the names should be saved in a file in file system. KGEN uses Fortran
WRITE statement if a variable is a intrinsic type such as integer or real or
explicit shape array.
If a variable is not an intrinsic type, such as pointer, implicit type array, derived type, etc.,
KGEN creates a subroutine for saving the variable. The subroutine may
contains calls to another subroutine for data saving in case that the variable
is not intrinsic type such as pointer type or derived type.

Once all names are resolved, KGEN generates source files for
instrumentation. Instrumented source file is a superset of original
source file that have additional codes for saving the content of
input and output data. KGEN also generates kernel
source files which are a subset of the original files.
However, the files also have additional codes for reading input
and output data from data files generated from instrumentation and for
verifying the output from kernel. In addition, it contains timing
measurement codes.




Most of the burden of manual kernel extraction is automated by the static static analysis of application source code that KGEN performs.  However several tasks benefit from a modest amount of user input. For example, user input is helpful in pruning the source tree that otherwise KGEN must search.


\subsection{Internal Structure}

We next describe the internal structure of KGEN.  KGEN is a command-line Python tool based on F2PY Fortran parser~\cite{peterson2009f2py}. A functional diagram of KGEN is provided in Figure \ref{fig:kgen-st}.  The core modules of KGEN include: a Name Resolver, Source file Generator, and a Makefile Generator.  We next describe the function of each core module 

%We next describe the internal structure KGEN.   KGEN is a command line Python tool based on F2PY Fortran parser~\cite{peterson2009f2py}.  KGEN, which utilizes Python 2.6 or greater, is pubically accessible through on GitHub ~\cite{kgen}. All source code, with the exception of Python and a CPP or FPP preprocessor. is provided in the release.  Note that a modified version F2PY, which provides support for several Fortran 2003 features, is also included.  A functional diagram of KGEN is provided in Figure \ref{fig:kgen-st}.  The core modules of KGEN include: a Name Resolver, Module Resolver, Kernel Data Generator, Source file Generator, and a Makefile Generator.  We next describe the function of each core module 

\begin{figure}
\centering
\includegraphics[width=2.8in]{KGEN_ST.png}
\caption{The internal structure of KGEN.}
\label{fig:kgen-st}
\end{figure}


%KGEN structure is shown in Figure~\ref{fig:kgen-st}. There are several sub-modules in KGEN that collectively enables kernel extraction.

\subsubsection{Name Resolver}

Name resolver is a core function of KGEN that selects Fortran source lines to be extracted.
Name in this context is a generic identifier that represents various entities in Fortran source code.
For example, a name could represent a variable, Function, Module, etc.
KGEN utilizes F2PY Parsing result that is encapsulated in a tree data structure per each source file.
Name Resolver scans a Fortran statement and finds names in the
statement to be resolved. Once it collects the names to be resolved,
it recursively checks if a parent statement can resolve each name.
 To eliminate unnecessary search, Name resolver limits
the type of resolving statement  that matches to the type of name
search. For example, if a name of the search is from Fortran call statement,
KGEN only searches Fortran subroutine statement because only Fortran
subroutine can be called in Fortran Call statement. 
Name Resolver also allows user of specifying names that should be excluded from name resolution.
This feature can greatly reduce the amount of name resolving when the excluded name is 
a part of external library and the library is not needed for the extracted kernel.
Once a
statement is found as a resolution for a name search, the statement
is marked to be extracted. If the resolving statement contains names to be resolved again,
all names in the resolving statement should be resolved too. That resolving process
continues until there is no name remained to be resolved.

Name Resolver also does name resolution throughout multiple Fortran modules. This feature is implemented
through Fortran Use statement that contains a module name and optionally names to be used from the module.
When Name Resolver found that a Fortran Use statement can resolve a name search,
Name Resolver tries to locate a source file containing the resolving module based on user-provided search paths.
Once Name Resolver finds a source file for the
resolving module, it further looks for a statement that actually
resolves the original name search in the module. Name Resolver supports renaming feature of
Fortran Use statement where a name in a module is renamed in Fortran Use statement.
Name Resolver utilizes Fortran only keyword in
Use statement to eliminate unnecessary search for modules. Name Resolver also
uses module caches to prevent multiple reading of the same module.

\subsubsection{Source file Generator}


There are two types of source file generators: kernel and instrumentation. Kernel is essentially stripped version of original
source files that only contains information required to make a kernel as a stand-alone application.
During name resolving task done by Name Resolver,
Fortran statements that are required for generated kernel to be a
stand-alone executable are marked for kernel generation. Source file Generator
preservers file names and Fortran source code structure from the
original application so that generated kernel does not introduce
spurious effect from restructuring source code. In addition to
source files extracted from the original application, Source file Generator adds
two additional files: kernel driver and utility. Kernel driver
makes a call to the parent subprogram of the call-site.
Only dummy arguments of the parent subprogram relevant to the kernel remains. This helps to 
remove unnecessary name resolutions.
 Kernel driver also makes calls to data saving routines in external modules.
The routines save the content of variables in the module that are used by the extracted kernel directly or indirectly.
These calls ensure that the extracted kernel has the same state to the original application 
just before calling to the kernel.
In a kernel file, Source file Generator also adds additional codes for reading
data from external file. These data are used to populate content
of subprogram argument and to verify the output from the extracted
kernel by comparing them to what are read from external file.

To generate state data for driving the kernel execution and verifying the result from the execution,
 Source file Generator modifies some of original source files by adding
codes for saving data before and after the call-site.
User needs to replace
the original source files with the modified source
files and to build/execute the instrumented application to
generate input/output data.
Input data are saved before the call-site and output data after the call-site.
In addition, it also searches external variables
 that are not defined but used in the kernel subprogram. All
 external variables are treated as INOUT intent type.
For external variables that could generate side-effects, Source file Generator saves the
variables at the same name space where the variables are declared.
For example, an external module contains a variable used in a kernel,
the code for saving the variable is located at the external module. The
code is called before executing the call-site which ensures that all the
content of external variables are the same to the original application.
When a variable is Fortran derived type, Source file Generator saves the content of each components recursively, which
is generally called as "deep-copy." If a component of a derived type variable is another derived type 
defined in a different source file, Source file Generator calls data saving subroutine for the component derived type.
This process ends when there is no more derived type component.
To limit the amount of data generation from MPI-application, Source file Generator maintain a list
of MPI ranks that will only save data. In addition, it can further limit data generation
by only allowing specific invocations of the kernel, where invocation information is generally provided by user
through KGEN command line flags.

%
%
%Kernel Data Generator analyses Fortran Intent attribute of
% Fortran subprogram arguments. There are three types of the attribute:IN, OUT and INOUT.
% The attribute type indicates the location of data saving near callsite in the source code.
% In case of IN type, variables used in statement of call-site as arguments are saved before
% the call-site statement. Variables of OUT types are saved after the call-site statement.
% Variables of INOUT are saved both of before and after the call-site.
% 


\subsubsection{Makefile Generator}
Makefile Generator generates Kernel makefile and Instrumentation makefile.
Kernel makefile is to help user for building and executing the extracted kernel.
During name resolution, Name Resolver saves dependency information for kernel
compilation and uses them for generating the kernel makefile.
Instrumentation makefile is to help user for saving kernel data into file system through
replacing original files
with instrumented files and building/executing the application. However,
user needs to provide enough information through command line flags
 for Makefile Generator to generate the contents of Instrumentation makefile.




% I want to move user interface material into the Example Workflow section.  However for now just include it here until I do.
%\input{UI}

\subsection{Example Workflow}

In this section, we describe the general KGEN workflow applied to the simple example in Figure \ref{fig:org-program}.  The KGEN workflow involves several steps, including preparing the original application for extraction, extracting the kernel, and generation of the state files.  

While KGEN is able to automate a great deal of program analysis, it does either benefit from or require some degree of user intervention.  The required user intervention includes the specification of the call-site, the definition of CPP variables, and specification of the source directories.  Optional user intervention includes the ability to prune the call tree to simplify the construction of the concrete syntax tree, and to limit the amount of state data that is collected.  

There are two methods to specify the call-site for which to extract a kernel.  A KGEN ``callsite'' directive is provided and has the following form:
 
\begin{displayquote}
!\$kgen callsite  callsite\_name
\end{displayquote}

This is the method that is used in the example from Figure \ref{fig:org-program}.  An alternative command-line option is also provided and avoids the need to modify source code.  KGEN is executed on the target source file using the following command:

\begin{displayquote}
python kgen.py [KGEN flags] sourcefile[:callsite\_identifier]
\end{displayquote}

The {\it sourcefile} is the source file that contains the call-site.  The call-site can be specified using a list of colon separate namespaces.  The call side\_identifier is of the form 

\begin{displayquote}
{\it module\_name:subprogram\_name:kernel\_subprogram\_name}.
\end{displayquote}

 The  {\it callsite\_identifier} of the example code in Figure \ref{fig:org-program}, would be the string``update\_mod:update:calc'', because the call to subroutine {\it calc} is located in the subroutine {\it update} which is located in the module {\it update\_mod}.  Additional KGEN flags are provided to further limit the scope of collected data.  Note that the call to subroutine {\it calc} is both nested in a loop and called from a parallel MPI program.  KGEN provides flags to limit the capture of state information to particular MPI ranks or particular invocation of a subroutine.  For example,  ``- -mpi rank=0:1'' flag limits the capture of state information from the {\it calc} subroutine to MPI ranks 0 and 1, while the flag
``- -invocation=1,3'' indicates KGEN should only save data from the 1st and 3rd call to {\it calc.}
KGEN also provides the ability to prune the source tree. The ability to prune can greatly simplify the analysis that KGEN must perform and limit the resulting size of the extracted kernel.  For example in Figure \ref{fig:org-program}, the user may know that call to the subroutine {\it print\_msg'} may not be relevant to the generation of the kernel.  The ability to prune is particular important in the case where the application uses a large external scientific library like NetCDF\cite{netcdf} or HDF5\cite{hdf5}, which may have a very deep call-trees.  In addition to the previously described KGEN specific capabilities, KGEN also provides several capabilities that are typically found on all Fortran and C compilers.  The ``-I'' flags sets the search path that KGEN uses look for relevant source code, while the ``-D'' argument allows for the setting of CPP variables.  

%A shell script that extracts ``calc'' subroutine as a kernel from the sample program in Figure~\ref{fig:org-program} is shown in Figure~\ref{fig:sample-script}.
%
%Once KGEN completes its execution, it creates two sub-directories in KGEN output directory.
%The output directory and the contents in the directory is shown in Figure~\ref{fig:output-folder}
%The screen output after running the shell script is shown in Figure~\ref{fig:scrout}.
%
%In ``kernel" sub-directory, all kernel source files and kernel Makefile are generated. In ``state" sub-directory, all instrumented source files
%and instrumentation Makefile are generated. User can build and/or
%execute the generated kernel by running ``make" command in "kernel"
%sub-directory. In the same way, user can generate kernel input/output
%data by running ``make" command in ``state" sub-directory. However user
%needs to provide enough information for the instrumentation through
%command line flags to make ``makefile" in ``state" sub-directory works properly.
%If instrumentation is successful, kernel data files will be created in ``kernel" sub-directory.

%\subsubsection{Preparing Original Application}

%KGEN reads the original application and extracts a part of codes specified by user. However, in general, there is additional information that user needs to provide. One type of information is paths that are used to search another module or included files used in a source file. Another type of information is macro definitions. While it is common that all source files uses the same macro definitions and inclusion paths, there are cases that each source files have different macro definitions and/or inclusion paths. For example, if a application includes an external library in its source folders, the external library may need different set of macro and inclusion path information. Collecting those macro definitions and inclusion paths could be time-consuming manual tasks. However, in many cases, those information can be systematically collected from a certain type of log files generated from building the application.  For example, CESM generates log files for each components and external libraries. A simple log parsing script would be enough to generate  INI inclusion file for KGEN.

%\subsubsection{Kernel Extraction}

Let now examine the extraction of the {\it calc} kernel in detail. A script to perform the extraction is provided in Figure \ref{fig:sample-script}.  Note that in addition to the ``--mpi'' and ``--invocation'' flags described previously it also uses two INI \cite{iniformat} files {\it include.ini} and {\it exclude.ini}.  Figure~\ref{fig:ini-files} illustrates the contents of ``include.ini'' and ``exclude.ini'' used in a shell script of Figure~\ref{fig:sample-script}.  There are two paths under ``include'' section of ``include.ini''. The first line lets KGEN know where to find source files other than a call-site
source file given in command-line. Second line is to specify the location of the ``mpif.h'' include file. Entries in the  ``exclude.ini'' tells KGEN to exclude a name of ``print\_msg''  when creating the concrete syntax tree and to replace the call with a Fortran comment in the transformed source. 

%Kernel extraction is done by executing KGEN with a command-line argument and optional command line flags. Mandatory information for kernel extraction is the name of source file that contains a call-site statement. As we are generally interested in extracting a kernel from the large application, additional information should be provided too: Inclusion paths, macro definitions, the list of names to be skipped during extraction, MPI ranks that data is generated from, etc.

%Figure~\ref{fig:sample-script} shows a shell script that extracts ``calc'' subroutine as a kernel from the sample program in Figure~\ref{fig:org-program}. ``-i'' flag is for providing module search paths and macro definitions. ``-e'' flags is for providing names to be excluded during name resolution. ``ranks'' sub-flag in ``- -mpi'' flag is to select MPI ranks that saved data from. ``- -ordinal-numbers'' flag indicates when KGEN saves data from counting the number calls to the kernel. ``repeat'' sub-flag in ``- -timing'' flag is to set the number of kernel execution between measuring timing, which increases the resolution of measurement. Lastly, a path to a file locates a source file that contains a call-site.

\begin{figure}[h]
\centering
\begin{lstlisting}[frame=single,language=bash,numbers=left,showstringspaces=false,
numbersep=-7pt,basicstyle=\scriptsize] 
  !#/bin/bash
  # a shell script to extract a sample kernel

  python kgen.py\
   -i include.ini\
   -e exclude.ini\
   --mpi ranks=0:1\
   --invocation 1,3\
   ./update_mod.F90
\end{lstlisting}
\caption{A shell script used for extracting a kernel from the sample program in Figure~\ref{fig:org-program}.}
\label{fig:sample-script}
\end{figure}

%   --state-build cmds="cd ${SRC_DIR}; make build"\
%   --state-run cmds="cd ${SRC_DIR}; make run"\



\begin{figure}[h]
\centering
\includegraphics[width=3.in]{INI_FILES.png}
\caption{``include.ini'' file for macros and inclusion paths, and ``exclude.ini'' file for excluding ``print\_msg'' from searching }
\label{fig:ini-files}
\end{figure}

%\begin{figure}[h]
%\centering
%\begin{lstlisting}[frame=single,language=bash,numbers=left,showstringspaces=false,
%numbersep=-7pt,basicstyle=\scriptsize] 
%   ; include.ini
%   [include]
%   ./src =
%   /mpi/include =
%
%   [macro]
%   ROW = 4
%   COL = 4
%\end{lstlisting}
%\caption{``include.ini'' file}
%\label{fig:include-ini}
%\end{figure}




%Figure ~\ref{fig:scrout} shows screen output from executing the script shown in Figure ~\ref{fig:sample-script}.

%From line 4 to 6, it shows that KGEN is reading ``calc\_mod.F90 source file to see if the name ``calc'' can be resolved by
%one of statement in the file. The resolution was triggered originally as well as directly from ``update\_mod.F90'' in this case. If there are multiple levels of
%module search, the names of source files read originally and directly may be different.

%\begin{figure}[h]
%\centering
%\begin{lstlisting}[frame=single,language=bash,numbers=left,showstringspaces=false,
%numbersep=-7pt,basicstyle=\scriptsize] 
%  Pre-processing is done
 % Reading ./src/update_mod.F90
 % Call-site location is found
 % Reading ./src/calc_mod.F90
 %  in the search of "calc" directly from update_mod.F90
%  Kernel information is collected
%  Instrumented files are generated
%  Kernel files are generated
%  Makefiles are generated
% Post-processing is done
%  Completed.
%\end{lstlisting}
%\caption{Screen output during extracting a kernel from the sample program in Figure ~\ref{fig:org-program}}
%\label{fig:scrout}
%\end{figure}

The execution of KGEN will generate an output directory structure illustrated in Figure \ref{fig:output-folder}. 
In the state sub-directory, a modified version of the input file ``update\_mod.F90'' is generated.  The instrumented file ``update\_mod.F90" is shown in Figure~\ref{fig:state-program}.
The original code structure is maintained while additional instrumentation code added to save input and output data.
On lines 719 and 720 in Figure~\ref{fig:state-program}, two input variables, ``i and j'', are saved into an external file and on line 738, 
one output variable,``output',' is saved after the call to {\it calc}.  

%The tasks of replacing and rebuilding can be automated by running ``make'' in ``state'' directory by using ``Makefile'' in the directory.

In the kernel sub-directory, there are several types of files are created. ``calc\_mod.F90'' and ``update\_mod.F90'' are extracted from
original source files. Statements only required to successfully execute the stand-alone kernel are remained. KGEN creates two
additional files ``kernel\_driver.f90'' , which executes the transformed ``update'' subroutine and ``kgen\_utils.f90'' that provides several utility functions including verification routines.

Figure~\ref{fig:kernel-program} shows three critical source files from the  ``kernel'' sub-directory. The ``kernel\_driver.f90'' is an entry point for kernel execution
that calls the parent of the call-site.  On lines 110, 111, and 114 in Figure \ref{fig:kernel-program} three variables are read from a state data file,
that correspond to lines of 719, 720, and 738 in Figure~\ref{fig:state-program} which captures the state data. After the call to kernel in line 118, there are several 
additional codes for output verification and timing measurement. 
The ``calc\_mod.F90'' files contains the computational kernel {\it calc}.  Note that only code necessary to execute the kernel is include.  In particular, the subroutine ``print\_msg'' is commented out on line 23 and 25 in Figure \ref{fig:kernel-program}.

%This file contains codes that are required to make the kernel executable. In additoion, this file also remove cods if user specified to do so. For example, ``print\_msg'' is excluded when the kernel is generated as shown in Figure~\ref{fig:sample-script}. Therefore, line 23 and 25 are commented out and ``print\_out'' subroutine in the original source file is not shown in this generated file. 

\begin{figure}[h]
\centering
\includegraphics[width=3.in]{OUTPUT_FOLDER.png}
\caption{Outputs from kernel extraction. ``update\_mod.F90'' in ``state'' folder contains codes for data generation. Files in ``kernel'' folder produce a stand-alone executable kernel. }
\label{fig:output-folder}
\end{figure}

 


%Files in ``state'' directory of Figure~\ref{fig:output-folder} are needed eventually to generate ``calc.*.*'' data files in ``kernel'' directory. To generate the data files, user needs to replace ``update\_mod.F90'' in the original sample program with one in ``state'' directory shown in Figure~\ref{fig:state-program}.

%Files whose name starts with ``calc.'' are data files generated from running instrumented sample program with KGEN-instrumented source files in ``state'' directory. There are four data files in the ``kernel'' directory because two MPI ranks saves data when ``calc'' subprogram is called first and third time as defined in  Figure~\ref{fig:sample-script}. ``Makefile'' is another KGEN-generated file to build and run the kernel.


%``calc\_mod.F90'' in Figure~\ref{fig:kernel-program} is the source file in the extracted kernel. It shows that actually two Fortran statements having the names are commented out. In addition, ``print\_msg'' Fortran subroutine defnition in the original program shown in Figure~\ref{fig:org-program} is removed in the extracted kernel in Figure~\ref{fig:kernel-program}.

% \subsubsection{Input and Output Data Generation}

\begin{figure}
\centering
\includegraphics[width=3.in]{STATE_PROGRAM.png}
\caption{The instrumented source file. This instrumented file will replace the same file in a sample file in Figure ~\ref{fig:org-program}
to generate data for driving kernel execution and verification}
\label{fig:state-program}
\end{figure}

%Files in ``state'' directory of Figure~\ref{fig:output-folder} are needed eventually to generate ``calc.*.*'' data files in ``kernel'' directory. To generate the data files, user needs to replace ``update\_mod.F90'' in the original sample program with one in ``state'' directory shown in Figure~\ref{fig:state-program}. The original code structure is maintained while additonal codes that are added to save input and output data. In line 719 and 720 in Figure~\ref{fig:state-program}, two input variables, ``i and j'', are saved into an external file and in line 738,  one output variable,``output',' is saved after the call-site. The tasks of replacing and rebuilding can be automated by running ``make'' in ``state'' directory by using ``Makefile'' in the directory.
%This is only possible if user provides KGEN with flags having the commands to be executed in the ``Makefile.

%\subsubsection{Kernel Execution and Verification}

\begin{figure}[h]
\centering
\includegraphics[width=3.in]{KERNEL_PROGRAM.png}
\caption{The kernel files that are extracted from the sample program in Figure ~\ref{fig:org-program}.}
\label{fig:kernel-program}
\end{figure}

Once the modified versions have been generated by KGEN it is now possible to generate state data files.  The original application is now compiled using the new instrumented code from the state sub-directory and executed to generate the state files.  Using the state files as input, the standalone kernel can now be compiled and executed.
